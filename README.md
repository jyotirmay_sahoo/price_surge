Summary:

Surge pricing, dynamic pricing, time-based pricing or demand pricing is an algorithmically fuelled pricing strategy, where businesses adjust the prices of their offerings to account for changing demand or circumstances. It works on the principle of maximizing the profit based on time-based opportunity.

In the context of Swiggy, dynamic pricing on delivery fees would be helpful to have supply-demand balance between the delivery partners’ availability and order demands, based on circumstances like :-
Adverse weather conditions (like heavy rains etc) 
Unavailability of Delivery partners during specific time periods (late nights or early mornings)
Countering high order demand during peak hours (like during lunch & dinner time)

It would give flexibility in terms of choice to the customers to pay the surge price during rush hours or wait for the demand to become low and surge price to go down.

Parameters/factors affecting the Price Surge include:

Weather Conditions - Adverse weather conditions like heavy rainfall or scorching heat, results in price surge
Availability of Delivery Partners - When there is shortage of delivery partners like in night time, or early morning, this increases the Price surge
Order Demand - On having a large number of orders in the region we will increase the delivery cost, and vice versa


Pros & Cons of Surge Pricing

Pros
Delivery partners can be paid higher wages during periods of high demand
Dynamic pricing would allow the business to sustain even in downtimes

Cons
Gives rise to the risk that customers might feel cheated for being asked to pay more.

	








Technical Implementation:

Tech Stack:

Essentials
Technology
API Testing Tool
Postman
API Service
AWS API Gateway
Backend programming Language
Golang
Serverless compute service
AWS Lambda
Database
 DynamoDb



In this project, we will use Dynamodb to get data related to the weather and DE(Delivery Executive) availability, lambda function for serverless computing along with API gateway to deploy our microservice written in Golang. We will use Postman for testing the microservice. 

In DDB, we will store the region id, number of available Delivery executives in that region, weather condition on a scale of 0 to 10 (where 0 means normal weather) for every 30 minutes and the current number of orders in execution in that region at that time frame. Region id will be the partition key and time will be the sort key for our DDB. For now, we will use dummy data for this. The main reason for using a DDB table here is to test our microservice. In a real world scenario, we would have fetched the weather data and DE availability data from API.  

We will implement our algorithm in golang and its deployment will be done through lambda function. Then we will expose our microservice through an API gateway. 
We will take a constant delivery time (i.e. 25 mins) for now.

We will test it using the Postman tool. To get the percentage price surge, we need to pass the region id and the price of the order as a parameter in the HTTP request.




Flowchart:




Uses:

It would help in matching the delivery executives’ efforts with the demand from consumers.
It would help in curbing Demand-Supply gap during downtimes & rush hours and would help Swiggy in grabbing opportunities during both phases.
It will help maximize the business revenue for Swiggy.
It would help the delivery executives step up their earnings as they get a certain percentage of the total delivery fare as earnings. 
It also has a long-term benefit as it communicates the delivery executives when they should, in general, get on roads for planned events, holidays or periods of expected high demand like poor weather.

To conclude, surge pricing would play a major role in cutting down the idle time and making unit economics profitable for Swiggy.



5 factors

Weather - 30
Availability - 25
Road Condition - 20
Demand - 15
Fuel Price - 10


base price = 100

fuel price surge = (new price - base price) * weightage
weightage = normal delivery charges * (percentage / 100)

weather = (factor) * weightage
** factor = [0,5]

Demand > Morning
DE Availability > Morning

Demand < Night
DE Availability < Night

Night > Morning

(Time)
Morning - 30%
Night - 70%



Availability = (factor) * weightage * time-weightage
**factor = (Normal Condition Availability - Current Availability) %




Distance Prameter = (Actual Distance Traveled - Expected Distance Traveled) %
DE Effort Parameter = (Actual DE Effort - Expected DE Effort) %

Road Condition = (Distance Prameter) * DE Effort Parameter * weightage



Demand = (factor) * weightage * time-weightage
**factor = (Normal Condition Demand - Current Demand) %



Normal Availabilty DE = 25
Current Availability DE = 20

Normal Demand Order = 50
Current Order Demand = 60

Base Price = 100
Current Fuel Price = 105

Weather Condition = Rainy = [2.5]

Expected Distance to be Travelled = 5km
Actual Distance Travelled = 7km

Base Charges = Rs35

Price Surge = ?

Price Surge due to fuel = (105 - 100)% + 1 = 1.05

Price Surge due to Weather Condition = (2.5 / 5)% + 1 = 1.5

Price Surge due to DE Availabilty = (25 - 20)% + 1 = 1.2 

Price Surge due to Order Demand = (60 - 50)% + 1 = 1.2

Price Surge due to Road Condition = (7- 5)% + 1 = 1.4


Price Surge = 35 * (1.05 * 0.1 + 1.5 * 0.3 + 1.2 * 0.25 + 1.2 * 0.15 + 1.4 * 0.2)
		= 35 * (1.315)
		= Rs 46.02


Price Surge = [Base Price, 1.5 * Base Price]





Data Modelling

Region
Day & Time  // Weekday -3:08,   <Day Type> <Time>
Normal DE Availability
Normal Order Demand
Current DE Availability
Current Order Demand
Current Weather Condition
Current Fuel Price

