package main

import (
	"github.com/aws/aws-lambda-go/events"
	runtime "github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"price_surge/handlers"
)

var dynamo *dynamodb.DynamoDB

const TABLE_NAME1 = "Region"
const TABLE_NAME2 = "Order_Details"


func connectDynamo() (db *dynamodb.DynamoDB) {
	return dynamodb.New(session.Must(session.NewSession(&aws.Config{
		Region: aws.String("us-east-1"),
	})))
}

func init() {
	dynamo = connectDynamo()
}

func main() {
	runtime.Start(handleRequest)
}
func handleRequest(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	switch request.HTTPMethod {
	case "GET":
		return handlers.GetDeliveryPrice(request, TABLE_NAME1, TABLE_NAME2, dynamo)
	default:
		return handlers.UnhandledMethod()
	}
}