package handlers

import (
	"errors"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"net/http"
	"price_surge/algos"
	"strconv"
	"time"
)

var (
	ErrorFailedToUnmarshalRecord = "failed to unmarshal record"
	ErrorMethodNotAllowed = "method Not allowed"
	ErrorFailedToFetchRecord     = "failed to fetch record"
)

type Result struct {
	BasePrice float64 `json:"baseprice"`
	DeliveryPrice float64 `json:"deliveryprice"`
	PercentageSurge float64 `json:"percentage_surge"`
}

type Region struct {
	Region_Id               int
	Time                    string
	Normal_Order_Demand     float64
	Normal_DE_Availability  float64
	Current_Order_Demand    float64
	Current_DE_Availability float64
	Current_Fuel_Price      float64
	Current_Weather_Factor  float64
}

type Order_Details struct {
	Order_Id                    int
	Expected_Distance_Travelled float64
	Region_Id                   int
	BaseDeliveryCharge float64
}

type ErrorBody struct {
	Err *string `json:"error,omitempty"`
}

func GetDeliveryPrice(req events.APIGatewayProxyRequest, table1 string, table2 string, ddbClient *dynamodb.DynamoDB) (
	*events.APIGatewayProxyResponse, error) {

	regionID,ok1 := req.QueryStringParameters["regid"]
	orderID, ok2 := req.QueryStringParameters["orderid"]

	if !ok1 || !ok2  {
		return apiResponse(http.StatusBadRequest, ErrorBody{Err: aws.String("region id or order id is null")})
	}

	if len(regionID)==0 || len(orderID)==0 {
		return apiResponse(http.StatusBadRequest, ErrorBody{Err: aws.String("region id and order id can't be empty")})
	}

	dayType := "Weekday"
	dt := time.Now()
	if temp:=int(dt.Weekday()); temp==0 || temp==6 {
		dayType = "Weekend"
	}
	timeString := dayType+"&"+getTime(dt)

	regionData, err := GetRegionData(regionID, table1, timeString, ddbClient)
	if err != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{Err: aws.String(err.Error())})
	}
	orderData, err := GetOrderData(orderID, regionID, table2, ddbClient)
	if err != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{Err: aws.String(err.Error())})
	}

	if regionData.Current_DE_Availability == 0 {
		return apiResponse(http.StatusBadRequest, ErrorBody{Err: aws.String("Can't deliver order now. No DE available")})
	}

	surge,err := algos.Calculate(orderData.BaseDeliveryCharge,regionData.Normal_DE_Availability,regionData.Current_DE_Availability,
		      regionData.Normal_Order_Demand,regionData.Current_Order_Demand, regionData.Current_Fuel_Price,
		      regionData.Current_Weather_Factor,orderData.Expected_Distance_Travelled)
	if err != nil {
		return apiResponse(http.StatusBadRequest, ErrorBody{Err: aws.String(err.Error())})
	}

	percentageSurge := 100*(surge-orderData.BaseDeliveryCharge)/orderData.BaseDeliveryCharge
	return apiResponse(http.StatusOK, Result{DeliveryPrice: surge, BasePrice: orderData.BaseDeliveryCharge,
		PercentageSurge: percentageSurge})

}

func getTime(dt time.Time) string {
	hr := dt.Hour()
	min := dt.Minute()
	hrString := strconv.Itoa(hr)
	if hr<10 {
		hrString = "0"+hrString
	}
	minString := "30"
	if min<30 {
		minString = "00"
	}
	return hrString+":"+minString
}

func GetRegionData(id string, tableName string, timeString string, dynamo *dynamodb.DynamoDB) (*Region, error) {
	result, err := dynamo.GetItem(&dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"Region_Id": {
				N: aws.String(id),
			},
			"Time": {
				S: aws.String(timeString),
			},
		},
		TableName: aws.String(tableName),
	})
	if err != nil {
		return nil,err
	}
	region := new(Region)
	err = dynamodbattribute.UnmarshalMap(result.Item, region)
	if err != nil {
		return nil,errors.New(ErrorFailedToUnmarshalRecord)
	}
	return region,nil
}
func GetOrderData(id string, rid string, tableName string, dynamo *dynamodb.DynamoDB) (*Order_Details, error)  {
	result, err := dynamo.GetItem(&dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"Order_Id": {
				N: aws.String(id),
			},
		},
		TableName: aws.String(tableName),
	})
	if err != nil {
		return nil,errors.New(ErrorFailedToFetchRecord)
	}
	order := new(Order_Details)
	err = dynamodbattribute.UnmarshalMap(result.Item, order)
	if err != nil {
		return nil,errors.New(ErrorFailedToUnmarshalRecord)
	}
	if strconv.Itoa(order.Region_Id)!=rid {
		return nil,errors.New("Region id doesn't match with order region id")
	}
	return order,nil
}

func UnhandledMethod() (*events.APIGatewayProxyResponse, error) {
	return apiResponse(http.StatusMethodNotAllowed, ErrorBody{Err: aws.String(ErrorMethodNotAllowed)})
}

