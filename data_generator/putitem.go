package main

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"math/rand"
	"price_surge/handlers"
	"strconv"
)

var dynamo *dynamodb.DynamoDB

const TABLE_NAME1 = "Region"
const TABLE_NAME2 = "Order_Details"

func PutRegionData(region handlers.Region) {
	_, err := dynamo.PutItem(&dynamodb.PutItemInput{
		Item: map[string]*dynamodb.AttributeValue{
			"Region_Id": {
				N: aws.String(strconv.Itoa(region.Region_Id)),
			},
			"Time": {
				S: aws.String(region.Time),
			},
			"Normal_Order_Demand": {
				N: aws.String(strconv.FormatFloat(region.Normal_Order_Demand,'E',-1,64)),
			},
			"Normal_DE_Availability": {
				N: aws.String(strconv.FormatFloat(region.Normal_DE_Availability,'E',-1,64)),
			},
			"Current_Order_Demand": {
				N: aws.String(strconv.FormatFloat(region.Current_Order_Demand,'E',-1,64)),
			},
			"Current_DE_Availability": {
				N: aws.String(strconv.FormatFloat(region.Current_DE_Availability,'E',-1,64)),
			},
			"Current_Fuel_Price": {
				N: aws.String(strconv.FormatFloat(region.Current_Fuel_Price,'E',-1,64)),
			},
			"Current_Weather_Factor": {
				N: aws.String(strconv.FormatFloat(region.Current_Weather_Factor,'E',-1,64)),
			},
		},
		TableName: aws.String(TABLE_NAME1),
	})
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			fmt.Println(aerr.Error())
		}
	}
}


func PutOrderData(order handlers.Order_Details) {
	_, err := dynamo.PutItem(&dynamodb.PutItemInput{
		Item: map[string]*dynamodb.AttributeValue{
			"Order_Id": {
				N: aws.String(strconv.Itoa(order.Order_Id)),
			},
			"BaseDeliveryCharge": {
				N: aws.String(strconv.FormatFloat(order.BaseDeliveryCharge,'E',-1,64)),
			},
			"Expected_Distance_Travelled": {
				N: aws.String(strconv.FormatFloat(order.Expected_Distance_Travelled,'E',-1,64)),
			},
			"Region_Id": {
				N: aws.String(strconv.Itoa(order.Region_Id)),
			},
		},
		TableName: aws.String(TABLE_NAME2),
	})
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			fmt.Println(aerr.Error())
		}
	}
}

func connectDynamo() (db *dynamodb.DynamoDB) {
	return dynamodb.New(session.Must(session.NewSession(&aws.Config{
		Region: aws.String("us-east-1"),
	})))
}
func init() {
	dynamo = connectDynamo()
}

func generateRandom(a, b int) int {
	return a+rand.Intn(b-a)
}

func generateRandomFloat(a, b float64) float64 {
	return a+rand.Float64()*(b-a)
}

func main() {
	regions := []int{1,2,3,4}
	dayType := []string{"Weekend", "Weekday"}
	time := []string{"00:00","00:30","01:00","01:30","02:00","02:30","03:00","03:30","04:00","04:30","05:00","05:30",
		             "06:00","06:30","07:00","07:30","08:00","08:30","09:00","09:30","10:00","10:30","11:00","11:30",
		             "12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30",
		             "18:00","18:30","19:00","19:30","20:00","20:30","21:00","21:30","22:00","22:30","23:00","23:30"}

	var regiondata handlers.Region
	for _, id := range regions {
		for _, day := range dayType {
			for _, t := range time {
				regiondata = handlers.Region{id,day+"&"+t, float64(generateRandom(0, 100)),
					float64(generateRandom(0, 100)), float64(generateRandom(0, 100)),
					float64(generateRandom(0, 100)), float64(generateRandom(90, 120)),
					generateRandomFloat(0,5)}
				PutRegionData(regiondata)
			}
		}
	}

	var orderdata handlers.Order_Details
	for i := 1; i <= 100; i++ {
		orderdata = handlers.Order_Details{i,generateRandomFloat(0.1,10),
			generateRandom(1,4),generateRandomFloat(20,70)}
		PutOrderData(orderdata)
	}
}
