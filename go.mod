module price_surge

go 1.16

require (
	github.com/aws/aws-lambda-go v1.26.0
	github.com/aws/aws-sdk-go v1.40.17
	github.com/stretchr/testify v1.6.1 // indirect
)
