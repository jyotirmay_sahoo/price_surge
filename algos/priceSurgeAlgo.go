package algos

import (
	"errors"
	"fmt"
	"math"
)

const (
	WEATHER_CONDITION_WEIGHTAGE float64 = 0.30
	DE_AVAILABILITY_WEIGHTAGE   float64 = 0.30
	ORDER_DEMAND_WEIGHTAGE      float64 = 0.25
	FUEL_PRICE_WEIGHTAGE        float64 = 0.15

	WEATHER_CONDITION_SCALE float64 = 5.0
	PRICE_SURGE_MAX_LIMIT   float64 = 1.5
	BASE_FUEL_PRICE         float64 = 100
	BASE_DISTANCE           float64 = 5
	EXTRA_DISTANCE_CHARGES  float64 = 2
)

func InputValidation(baseDeliveryCharge, normalDEAvailabilty, currentDEAvailability,
	normalOrderDemand, currentOrderDemand, currentFuelPrice,
	weatherCondition, expectedDistanceTravelled float64) error {

	if baseDeliveryCharge < 0 || normalDEAvailabilty < 0 || currentDEAvailability < 0 ||
		normalOrderDemand < 0 || currentOrderDemand < 0 || currentFuelPrice < 0 ||
		weatherCondition < 0 || expectedDistanceTravelled < 0 {
		return errors.New("input Parameters must be Non-Negative")
	}

	if weatherCondition > WEATHER_CONDITION_SCALE {
		return errors.New("wheatherCondition Parameter must be in the given Scale Range")
	}

	return nil
}

func Calculate(baseDeliveryCharge, normalDEAvailabilty, currentDEAvailability,
	normalOrderDemand, currentOrderDemand, currentFuelPrice,
	weatherCondition, expectedDistanceTravelled float64) (float64, error) {

	var inputValidationError = InputValidation(baseDeliveryCharge, normalDEAvailabilty, currentDEAvailability,
		normalOrderDemand, currentOrderDemand, currentFuelPrice,
		weatherCondition, expectedDistanceTravelled)

	if inputValidationError != nil {
		return baseDeliveryCharge, inputValidationError
	}

	var fuelPriceSurgeFactor = 1 + ((currentFuelPrice - BASE_FUEL_PRICE) / BASE_FUEL_PRICE)
	fmt.Println("fuelPriceSurgeFactor = ", fuelPriceSurgeFactor)

	var wheatherConditionSurgeFactor = 1 + (weatherCondition / WEATHER_CONDITION_SCALE)
	fmt.Println("wheatherConditionSurgeFactor = ", wheatherConditionSurgeFactor)

	var DEAvailabilitySurgeFactor = 1 + ((normalDEAvailabilty - currentDEAvailability) / normalDEAvailabilty)
	fmt.Println("DEAvailabilitySurgeFactor = ", DEAvailabilitySurgeFactor)

	var orderDemandSurgeFactor float64
	if normalOrderDemand==0 {
		orderDemandSurgeFactor = 10
	} else {
		orderDemandSurgeFactor = 1 + ((currentOrderDemand - normalOrderDemand) / normalOrderDemand)
	}
	fmt.Println("orderDemandSurgeFactor = ", orderDemandSurgeFactor)

	var extraDistanceSurge = math.Max((expectedDistanceTravelled-BASE_DISTANCE)*EXTRA_DISTANCE_CHARGES, 0)
	fmt.Println("extraDistanceSurge = ", extraDistanceSurge)

	var priceSurgeValue = baseDeliveryCharge*((fuelPriceSurgeFactor*FUEL_PRICE_WEIGHTAGE)+
		(wheatherConditionSurgeFactor*WEATHER_CONDITION_WEIGHTAGE)+
		(orderDemandSurgeFactor*ORDER_DEMAND_WEIGHTAGE)+
		(DEAvailabilitySurgeFactor*DE_AVAILABILITY_WEIGHTAGE)) + extraDistanceSurge

	return math.Min(math.Max(priceSurgeValue, baseDeliveryCharge), baseDeliveryCharge*PRICE_SURGE_MAX_LIMIT), nil
}

// func main() {
// 	var baseDeliveryCharge, normalDEAvailabilty, currentDEAvailability,
// 		normalOrderDemand, currentOrderDemand, currentFuelPrice,
// 		weatherCondition, expectedDistanceTravelled float64

// 	baseDeliveryCharge = 35
// 	normalDEAvailabilty = 25
// 	currentDEAvailability = 20
// 	normalOrderDemand = 60
// 	currentOrderDemand = 70
// 	currentFuelPrice = 105
// 	weatherCondition = 0
// 	expectedDistanceTravelled = 7

// 	var priceSurgeValue, err = Calculate(baseDeliveryCharge, normalDEAvailabilty, currentDEAvailability,
// 		normalOrderDemand, currentOrderDemand, currentFuelPrice,
// 		weatherCondition, expectedDistanceTravelled)

// 	if err != nil {
// 		fmt.Println("Error Encounterred: ", err)
// 	} else {
// 		fmt.Println("Calculated Price Surge = ", priceSurgeValue)
// 	}
// }
