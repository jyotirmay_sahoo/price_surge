package algos

import "testing"

type PriceSurgeAlgoParameters struct {
	baseDeliveryCharge        float64
	normalDEAvailabilty       float64
	currentDEAvailability     float64
	normalOrderDemand         float64
	currentOrderDemand        float64
	currentFuelPrice          float64
	weatherCondition          float64
	expectedDistanceTravelled float64

	EXPECTED_OUTPUT float64
}

func TestInputData(t *testing.T) {
	var testValues = []PriceSurgeAlgoParameters{
		{baseDeliveryCharge: 35, normalDEAvailabilty: 25, currentDEAvailability: 20,
			normalOrderDemand: 60, currentOrderDemand: 70, currentFuelPrice: 105, weatherCondition: 0,
			expectedDistanceTravelled: 7, EXPECTED_OUTPUT: 42.82083333333334},
		{baseDeliveryCharge: 35, normalDEAvailabilty: 25, currentDEAvailability: 20,
			normalOrderDemand: 60, currentOrderDemand: 70, currentFuelPrice: 105, weatherCondition: 1,
			expectedDistanceTravelled: 7, EXPECTED_OUTPUT: 44.92083333333333},
		{baseDeliveryCharge: 35, normalDEAvailabilty: 25, currentDEAvailability: 25,
			normalOrderDemand: 60, currentOrderDemand: 70, currentFuelPrice: 105, weatherCondition: 1,
			expectedDistanceTravelled: 7, EXPECTED_OUTPUT: 42.82083333333333},
		{baseDeliveryCharge: 35, normalDEAvailabilty: 25, currentDEAvailability: 25,
			normalOrderDemand: 60, currentOrderDemand: 60, currentFuelPrice: 105, weatherCondition: 0,
			expectedDistanceTravelled: 5, EXPECTED_OUTPUT: 35.2625},
		{baseDeliveryCharge: 35, normalDEAvailabilty: 25, currentDEAvailability: 25,
			normalOrderDemand: 60, currentOrderDemand: 60, currentFuelPrice: 100, weatherCondition: 0,
			expectedDistanceTravelled: 5, EXPECTED_OUTPUT: 35},
		{baseDeliveryCharge: 35, normalDEAvailabilty: 25, currentDEAvailability: 25,
			normalOrderDemand: 30, currentOrderDemand: 60, currentFuelPrice: 100, weatherCondition: 5,
			expectedDistanceTravelled: 5, EXPECTED_OUTPUT: 52.5},
	}
	for _, test := range testValues {
		output, err := Calculate(test.baseDeliveryCharge, test.normalDEAvailabilty, test.currentDEAvailability,
			test.normalOrderDemand, test.currentOrderDemand, test.currentFuelPrice,
			test.weatherCondition, test.expectedDistanceTravelled)

		if err != nil {
			t.Error("Test Failed: Error Encountered: ", err)
		} else if output != test.EXPECTED_OUTPUT {
			t.Error("Test Failed: Inputted { baseDeliveryCharge:", test.baseDeliveryCharge, ", normalDEAvailabilty: ", test.normalDEAvailabilty,
				", normalOrderDemand: ", test.normalOrderDemand, ", currentOrderDemand: ", test.currentOrderDemand, ", currentFuelPrice: ",
				test.currentFuelPrice, ", weatherCondition: ", test.weatherCondition, ", expectedDistanceTravelled: ", test.expectedDistanceTravelled, "}, ",
				"Expected: ", test.EXPECTED_OUTPUT, ", Recieved: ", output)
		}
	}
}

func TestNegativeInputData(t *testing.T) {
	var test = PriceSurgeAlgoParameters{
		baseDeliveryCharge: 35, normalDEAvailabilty: -25, currentDEAvailability: 20,
		normalOrderDemand: 60, currentOrderDemand: 70, currentFuelPrice: 105, weatherCondition: 0,
		expectedDistanceTravelled: 7,
	}

	_, err := Calculate(test.baseDeliveryCharge, test.normalDEAvailabilty, test.currentDEAvailability,
		test.normalOrderDemand, test.currentOrderDemand, test.currentFuelPrice,
		test.weatherCondition, test.expectedDistanceTravelled)

	if err == nil {
		t.Error("Test Failed: Input Validation Check failed, No Error thrown for Negative Input Data ")
	}
}

func TestInvalidWeatherConditionParameter(t *testing.T) {
	var test = PriceSurgeAlgoParameters{
		baseDeliveryCharge: 35, normalDEAvailabilty: 25, currentDEAvailability: 20,
		normalOrderDemand: 60, currentOrderDemand: 70, currentFuelPrice: 105, weatherCondition: 6,
		expectedDistanceTravelled: 7,
	}

	_, err := Calculate(test.baseDeliveryCharge, test.normalDEAvailabilty, test.currentDEAvailability,
		test.normalOrderDemand, test.currentOrderDemand, test.currentFuelPrice,
		test.weatherCondition, test.expectedDistanceTravelled)

	if err == nil {
		t.Error("Test Failed: Input Validation Check failed, No Error thrown for Invalid Weather Condition Data ")
	}
}
